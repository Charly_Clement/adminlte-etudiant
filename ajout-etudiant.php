
<?php require 'header.php';

        $prenom     = isset($_POST['prenom'])    && !empty($_POST['prenom'])    ? $_POST['prenom']    : '';
        $nom        = isset($_POST['nom'])       && !empty($_POST['nom'])       ? $_POST['nom']       : '';
        $surnom     = isset($_POST['surnom'])    && !empty($_POST['surnom'])    ? $_POST['surnom']    : '';
        $telephone  = isset($_POST['telephone']) && !empty($_POST['telephone']) ? $_POST['telephone'] : '';
        $email      = isset($_POST['email'])     && !empty($_POST['email'])     ? $_POST['email']     : '';
        $submit     = isset($_POST['submit']);

        if ($submit) {
            try {
                $save = $pdo->prepare("INSERT INTO etudiant (prenom,nom,surnom,telephone,email)
                                            VALUES (:prenom,:nom,:surnom,:telephone,:email)");
                $save->execute(['prenom'=>$prenom,'nom'=>$nom,'surnom'=>$surnom,'telephone'=>$telephone,'email'=>$email]);

                header('Location: index.php');

            }
            catch (PDOException $e) {
                echo 'Error: '.$e->getMessage();
            }
        }

    ?>

    <div class="container my-5 bg-info w-50">
        <form method="post" class="p-3">
            <div class="form-group">
                <label for="prenom">Prénom</label>
                <input type="text" class="form-control" name="prenom" id="prenom"">
            </div>
            <div class="form-group">
                <label for="nom">Nom</label>
                <input type="text" class="form-control" name="nom" id="nom">
            </div>
            <div class="form-group">
                <label for="surnom">Surnom</label>
                <input type="text" class="form-control" name="surnom" id="surnom">
            </div>
            <div class="form-group">
                <label for="telephone">téléphone</label>
                <input type="tel" class="form-control" name="telephone" id="telephone">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="mail" class="form-control" name="email" id="email">
            </div>
            <div class="container">
                <div class="col-2 my-3 offset-10">
                    <input class="bg-white p-2 text-bold" type="submit" name="submit" value="Enregistrer">
                </div>
            </div>
        </form>
    </div><!-- /.container-fluid -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require 'footer.php' ?>
