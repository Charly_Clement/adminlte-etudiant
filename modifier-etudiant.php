
<?php require 'header.php';

    // AFFICHE L'ÉTUDIANT À MODIFIER
    $id_etudiant = $_GET['id'];

    try {
        $modifier_etudiant = $pdo->prepare("SELECT * FROM etudiant WHERE id_etudiant=?");
        $modifier_etudiant -> execute([$id_etudiant]);
        $modifier_etudiant = $modifier_etudiant -> fetchAll();
        $modifier_etudiant = $modifier_etudiant[0];

    }
    catch(PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }

    $prenom     = isset($_POST['prenom'])    && !empty($_POST['prenom'])    ? $_POST['prenom']    : '';
    $nom        = isset($_POST['nom'])       && !empty($_POST['nom'])       ? $_POST['nom']       : '';
    $surnom     = isset($_POST['surnom'])    && !empty($_POST['surnom'])    ? $_POST['surnom']    : '';
    $telephone  = isset($_POST['telephone']) && !empty($_POST['telephone']) ? $_POST['telephone'] : '';
    $email      = isset($_POST['email'])     && !empty($_POST['email'])     ? $_POST['email']     : '';
    $submit     = isset($_POST['submit']);

    if ($submit) {
        try {
            $requete = $pdo->prepare("UPDATE etudiant SET prenom='$prenom', nom='$nom', surnom='$surnom', telephone='$telephone', email='$email' WHERE id_etudiant=$id_etudiant");
            $requete -> execute([$id_etudiant]);
            header('Location:etudiant.php');

        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    // SUPPRESSION D'UN ÉTUDIANT
    $sup = isset($_GET['sup']) && !empty($_GET['sup']) ? $_GET['sup'] : '';

    if ($sup == 'ok') {
        if ($id_etudiant != null) {
            
            try {

                $requete = $pdo->prepare("DELETE FROM etudiant WHERE id_etudiant=?");
                $requete -> execute([$id_etudiant]);
                header('Location: etudiant.php');

            }
            catch(PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        }
    }

?>

<div class="container my-5 bg-info w-50">
    <form method="post" class="p-3">
        <div class="form-group">
            <label for="prenom">Prénom</label>
            <input type="text" class="form-control" id="prenom" name="prenom" value="<?php echo $modifier_etudiant['prenom']; ?>">
        </div>
        <div class="form-group">
            <label for="nom">Nom</label>
            <input type="text" class="form-control" id="nom" name="nom" value="<?php echo $modifier_etudiant['nom']; ?>">
        </div>
        <div class="form-group">
            <label for="surnom">Surnom</label>
            <input type="text" class="form-control" id="surnom" name="surnom" value="<?php echo $modifier_etudiant['surnom']; ?>">
        </div>
        <div class="form-group">
            <label for="telephone">téléphone</label>
            <input type="tel" class="form-control" id="telephone" name="telephone" value="<?php echo $modifier_etudiant['telephone']; ?>">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" value="<?php echo $modifier_etudiant['email']; ?>">
        </div>

        <!-- BOUTONS -->
        <div class="container">
            <div class="row">
                <div class="col-2 mt-4 offset-8">
                    <?php echo '<a href="?sup=ok&id='.$modifier_etudiant['id_etudiant'].'" class="text-decoration-none bg-danger text-black text-bold mx-3 px-4 py-2">Supprimer</a>' ?>
                </div>
                <div class="col-2 my-3">
                    <input class="bg-success p-2 text-black text-bold" type="submit" name="submit" value="Enregistrer">
                </div>
            </div>
        </div>

    </form>
</div>

<?php require 'footer.php' ?>
