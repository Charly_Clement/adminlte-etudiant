<?php require 'header.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Liste des étudiants</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.html">Accueil</a></li>
              <li class="breadcrumb-item active">Tableau de bord</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row">
          <a href="ajout-etudiant.php"><button type="button" class="btn btn-success">Ajouter un étudiant</button></a>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <?php

      try {
        $etudiants = $pdo->prepare("SELECT id_etudiant, prenom, nom, telephone, email FROM etudiant ");
        $etudiants->execute();
        $etudiants = $etudiants->fetchAll();

      }
      catch (PDOException $e) {
        echo 'Error: '.$e->getMessage();
      }

    ?>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <?php
          
            foreach ($etudiants as $etudiant) {

              echo '<div class="col-lg-3 col-6">
                    <div class="small-box bg-info">
                      <div class="inner">

                        

                        <h3>' . $etudiant['prenom'] . '</h3>

                        <p>élèves</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-user-plus"></i>
                      </div>
                      <a href="modifier-etudiant.php?id='.$etudiant['id_etudiant'].'" class="small-box-footer">Modifier <i class="fas fa-arrow-circle-right"></i></a>
                      
                    </div>
                  </div>';
            }
          ?>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php require 'footer.php' ?>
