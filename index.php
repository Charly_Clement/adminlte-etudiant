
<?php require 'header.php' ?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tableau de bord</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.html">Accueil</a></li>
              <li class="breadcrumb-item active">Tableau de bord</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6">
              <div class="small-box bg-info">
                <div class="inner">

                    <?php
                      // AFFICHE LE NOMBRE D'ÉTUDIANT ENREGISTRÉS
                      try {
                        $nbEtudiant = $pdo->query("SELECT COUNT(*) FROM etudiant");
                        $nbEtudiant = $nbEtudiant->fetchColumn();
                      }
                      catch (PDOException $e) {
                          echo 'Error: '.$e->getMessage();
                      }
                    ?>

                    <h3><?php echo $nbEtudiant ?> </h3>
                  

                  <p>
                  <?php
                    if ($nbEtudiant < 2) {
                      echo 'élève';
                    }else {
                      echo 'élèves';
                    }
                  ?>
                    
                  </p>
                </div>
                <div class="icon">
                  <i class="fas fa-user-plus"></i>
                </div>
                <a href="etudiant.php" class="small-box-footer">Afficher la liste <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php require 'footer.php' ?> 
